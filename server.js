const createError = require('http-errors');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');
const logger = require('morgan');
const cors = require('cors'); 

const auth = require('./routes/api/auth');
const users = require('./routes/api/users');
const feedback = require('./routes/api/feedback');

const app = express();

// Bodyparser middleware
app.use( bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(logger('dev'));

// DB Config
const db = require('./config/keys').database;

// Connect to MongoDB
mongoose.connect( db, { 
		useNewUrlParser: true,
		useUnifiedTopology: true 
	})
	.then(() => console.log('MongoDB successfully connected'))
	.catch(err => console.log(err));

// Passport middleware
app.use(passport.initialize());

// Passport config
require('./config/passport')(passport);

// Routes
app.use('/api/users', users);
app.use('/api/auth', auth);
app.use('/api/feedback', feedback);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server up and running on port ${port} !`));
