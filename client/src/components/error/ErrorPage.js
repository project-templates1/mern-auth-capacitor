import React, { Component } from "react";
import { Link } from "react-router-dom";

class ErrorPage extends Component {
  render() {
    return (
      <div style={{ height: "75vh" }} className="container valign-wrapper">
        <div className="row">
          <div className="col s12 center-align">
            <h4>
              <span style={{ fontFamily: "monospace" }}> Page not found. </span>
            </h4>
            <p className="flow-text grey-text text-darken-1">
             Please check your URL and/or confirm you are logged in.
            </p>
            <br />
            <div className="col s6">
              <Link
                to="/login"
                style={{
                  width: "140px",
                  borderRadius: "3px",
                  letterSpacing: "1.5px"
                }}
                className="btn btn-large waves-effect waves-light hoverable blue accent-2"
              >
                Log In
              </Link>
            </div>
            <div className="col s6">
              <Link
                to="/register"
                style={{
                  width: "140px",
                  borderRadius: "3px",
                  letterSpacing: "1.5px"
                }}
                className="btn btn-large waves-effect waves-light hoverable blue accent-2"
              >
                Register
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ErrorPage;
