import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { submitFeedback } from '../../actions/feedbackActions';
import { FEEDBACK_TYPE } from '../../utils/constants';
import classnames from 'classnames';

const initState = {
	type: FEEDBACK_TYPE.COMPLIMENT,
	description: '',
	errors: {}
};

class Settings extends Component {
	constructor(props) {
		super(props);
		this.state = initState;
	}

	onChange = e => {
		console.log('e.target: ',e.target);
		this.setState({ [e.target.id]: e.target.value });
	};

	onSubmit = e => {
		e.preventDefault();

		const feedback = {
			userId: this.props.auth.user.id,
			type: this.state.type,
			description: this.state.description
		};

		this.props.submitFeedback(feedback,response => {
			alert(response.data.msg);
			this.setState(initState);
		});
	};

	render() {
		const { errors } = this.state;

		return (
			<div className="container">
				<div className="row">
					<div className="col s8 offset-s2">
						<Link to="/Dashboard" className="btn-flat waves-effect">
							<i className="material-icons left">keyboard_backspace</i> Back to
							Dashboard
						</Link>
						<div className="col s12" style={{ paddingLeft: "11.250px" }}>
							<h4>
								<b>Feedback</b>
							</h4>
						</div>
						<form noValidate onSubmit={this.onSubmit}>
							<div className="radio input-field col s12">
								<p>
									<label>
							    		<input className="with-gap" name="type" id="type" type="radio" value={FEEDBACK_TYPE.COMPLIMENT} 
											checked={this.state.type === FEEDBACK_TYPE.COMPLIMENT} 
											onChange={this.onChange} />
										<span>Compliment</span>
									</label>
							    </p>
							    <p>
									<label>
							    		<input className="with-gap" name="type" id="type" type="radio" value={FEEDBACK_TYPE.RECOMMENDATION} 
											checked={this.state.type === FEEDBACK_TYPE.RECOMMENDATION} 
											onChange={this.onChange} />
										<span>Recommendation</span>
									</label>
							    </p>
							    <p>
									<label>
							    		<input className="with-gap" name="type" id="type" type="radio" value={FEEDBACK_TYPE.BUG} 
											checked={this.state.type === FEEDBACK_TYPE.BUG} 
											onChange={this.onChange} />
										<span>Bug</span>
									</label>
							    </p>
							</div>
							<div className="input-field col s12">
								<textarea
									onChange={this.onChange}
									value={this.state.description}
									error={errors.description}
									id="description"
									className={classnames("materialize-textarea ", {
										invalid: errors.description
									})}
								/>
								<label htmlFor="description">Comment</label>
								<span className="red-text">{errors.description}</span>
							</div>
							<div className="col s12" style={{ paddingLeft: "11.250px" }}>
								<button
									style={{
										width: "150px",
										borderRadius: "3px",
										letterSpacing: "1.5px",
										marginTop: "1rem"
									}}
									type="submit"
									className="btn btn-large waves-effect waves-light hoverable blue accent-3"
								>
									Submit
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

Settings.propTypes = {
	submitFeedback: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
	auth: state.auth,
	errors: state.errors
});

export default connect(
 	mapStateToProps,
 	{ submitFeedback }
)(withRouter(Settings));
