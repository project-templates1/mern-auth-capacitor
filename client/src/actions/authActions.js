import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';

import { GET_ERRORS, SET_CURRENT_USER, USER_LOADING } from './types';

// Register User
const registerUser = (userData, history) => dispatch => {
	axios.post('/api/auth/register', userData)
		.then(res => history.push('/login'))
		.catch(err =>{
			dispatch({
				type: GET_ERRORS,
				payload: err
			})
		});
};

// Login - get user token
const loginUser = userData => dispatch => {
	axios.post('/api/auth/login', userData)
		.then(res => {
			// Set token to localStorage
			const { token } = res.data;
			localStorage.setItem('jwtToken', token);
			// Set token to Auth header
			setAuthToken(token);
			// Decode token to get user data
			const decoded = jwt_decode(token);
			// Set current user
			dispatch(setCurrentUser(decoded));
		})
		.catch(err =>
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		);
};

// User Change Password
const changePassword = (userData, history, cb) => dispatch => {
	axios.post('/api/users/changePassword', userData)
		.then(res => { 
			history.push('/changePassword');
			cb(true);
		})
		.catch(err =>{
			cb(err);
			dispatch({
				type: GET_ERRORS,
				payload: err
			})
		});
};

// Change User Info
const changeUserInfo = (userData, authUser, cb) => dispatch => {
	console.log('changeUserInfo - a: ',userData);
	axios.post('/api/users/changeUserInfo', userData)
		.then(res => {
			authUser.name = res.data.name;
			authUser.email = res.data.email;
			cb(true);
		})
		.catch(err =>{
			cb(err);
			dispatch({
				type: GET_ERRORS,
				payload: err
			})
		});
};

// Set logged in user
const setCurrentUser = decoded => {
	return {
		type: SET_CURRENT_USER,
		payload: decoded
	};
};

// User loading
const setUserLoading = () => {
	return {
		type: USER_LOADING
	};
};

// Log user out
const logoutUser = () => dispatch => {
	// Remove token from local storage
	localStorage.removeItem('jwtToken');
	// Remove auth header for future requests
	setAuthToken(false);
	// Set current user to empty object {} which will set isAuthenticated to false
	dispatch(setCurrentUser({}));
};

export {
	registerUser,
	loginUser,
	changePassword,
	changeUserInfo,
	setCurrentUser,
	setUserLoading,
	logoutUser
}
