import axios from 'axios';
import { GET_ERRORS } from './types';

// Register User
const submitFeedback = (feedback,cb) => dispatch => {
	axios.post('/api/feedback/submitFeedback', feedback)
		.then( response => {
			cb(response);
		})
		.catch(err => {
			dispatch({
				type: GET_ERRORS,
				payload: err
			})
			cb(err);
		})
};

export {
	submitFeedback
}