# mern-auth-capacitor

![Final App](https://i.postimg.cc/tybZb8dL/final-MERNAuth.gif)
Minimal full-stack MERN app with authentication using passport and JWTs.

This project uses the following technologies:

- [React](https://reactjs.org) and [React Router](https://reacttraining.com/react-router/) for frontend
- [Express](http://expressjs.com/) and [Node](https://nodejs.org/en/) for the backend
- [MongoDB](https://www.mongodb.com/) for the database
- [Redux](https://redux.js.org/basics/usagewithreact) for state management between React components

## Configuration

Make sure to add your `database` is setup appropriate for your MongoDB config and your create your own unique `secret` in`config/keys.js`.

```javascript
module.exports = {
  database: "mongodb://localhost/node-auth",
  secretOrKey: "secret"
};
```

## Quick Start
- Install Mongo: https://docs.mongodb.com/manual/administration/install-community/
- Install Express Generator: `npm install express-generator -g`

```javascript
// Install dependencies for server & client & electron
npm install && npm run client-install && npm run electron-install

// Re-add iOS folder
npx cap add ios

// Re-add Android folder
npx cap add android
// Electron does not need to be re-added on initial setup

// Run client & server with concurrently
npm run dev
// Server runs on http://localhost:5000 and client on http://localhost:3000

// Copies the cocde into the respective Capacitor projects
npx cap copy

// Opens in the respective platform
npx cap open ios | android | electron
```

The MERN-Auth portion of this is based on https://github.com/rishipr/mern-auth/
