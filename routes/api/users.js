const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const passport = require('passport');

// Load input validation
const validator = require('../../services/validator');


// Load User model
const User = require('../../models/User');

// Ensure all routes are private
router.use(passport.authenticate('jwt', { session: false}));

// @route POST api/users/changePassowrd
// @desc Change existing user's passowrd
// @access Private //TODO: how do I make this true?
router.post('/changePassword', (req, res) => {

	// Form validation
	const { errors, isValid } = validator.changePasswordInput(req.body);

	// Check validation
	if (!isValid) {
		return res.status(400).json(errors);
  	}

	User.findOne({ _id: req.body.id }).then(user => {
		if (!user) {
			return res.status(400).json({ email: 'User not found.' });
		} else {
			user.password = req.body.password;

			// Hash password before saving in database
			bcrypt.genSalt(10, (err, salt) => {
				bcrypt.hash(user.password, salt, (err, hash) => {
					console.log('---> err:',err);
					if (err) throw err;
					console.log('---> hash:',hash);
					user.password = hash;
					user.save()
						.then(user => {
							console.log('---> user:',user);
							res.json(user);
            			})
            			.catch(err => console.log(err));
        		});
      		});
    	}
  	});
});

// @route POST api/users/changeUserInfo
// @desc Change existing user's information
// @access Private //TODO: how do I make this true?
router.post('/changeUserInfo', (req, res) => {
	const userInfo = req.body;

	// Form validation
	const { errors, isValid } = validator.changeUserInfo(userInfo);

	// Check validation
	if (!isValid) {
		return res.status(400).json(errors);
  	}

	User.findOne({ _id: userInfo.id }).then(user => {
		if (!user) {
			return res.status(400).json({ email: 'User not found.' });
		} else {

			for (let prop in userInfo) {
				if (userInfo[prop] != null || userInfo[prop] !== '') {
					user[prop] = userInfo[prop];
				}
			}

			user.save()
				.then(user => {
					res.json(user);
    			})
    			.catch(err => console.log(err));
    	}
  	});
});

// @route GET api/users/getUsers
// @desc get either one or many users
// @access Private //TODO: how do I make this true?
router.get('/getUsers/:ids', async (req, res) => {
	let users = [];
	const ids = typeof req.params.ids === 'string' ? req.params.ids.split(',') : [];
	try {
		if (ids.length > 1){
			users = await User.find({ _id: ids }).select('-password');
		} else {
			const user = await User.findOne({ _id: ids[0] }).select('-password');
			users.push(user);
		}

		res.status(200).json(users);
	} catch (e) {
		res.status(400).json(e);
	}
});


module.exports = router;
